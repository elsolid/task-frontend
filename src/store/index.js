import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

const SET_GATEWAYS = 'SET_GATEWAYS'
const SET_SELECTED = 'SET_SELECTED'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    gateways: [],
    gw: {}
  },
  mutations: {
    [SET_GATEWAYS] (state, response) {
      state.gateways = response
      if (state.gw) {
        state.gw = state.gateways.find(g => g.id === state.gw.id)
      }
    },
    [SET_SELECTED] (state, response) {
      state.gw = response
    }
  },
  actions: {
    async getAllGateways ({ commit }) {
      await axios.get('/api/gateways')
        .then((res) => {
          if (res.status === 200) {
            commit(SET_GATEWAYS, res.data)
          }
        })
    },
    async createGateway ({ dispatch }, gateway) {
      await axios.post('/api/gateways', gateway)
        .then((res) => {
          if (res.status === 200) {
            dispatch('getAllGateways')
          }
        })
    },
    async editGateway ({ dispatch }, gateway) {
      await axios.put(`/api/gateways/${gateway.id}`, gateway)
        .then((res) => {
          if (res.status === 200) {
            dispatch('getAllGateways')
          }
        })
    },
    async deleteGateway ({ dispatch }, gateway) {
      await axios.delete(`/api/gateways/${gateway.id}`)
        .then((res) => {
          if (res.status === 200) {
            dispatch('getAllGateways')
          }
        })
    },

    // devices
    async createDevice ({ dispatch }, { gateway, device }) {
      await axios.post(`/api/devices/${gateway.id}`, device)
        .then((res) => {
          if (res.status === 200) {
            dispatch('getAllGateways')
          }
        })
    },
    async editDevice ({ dispatch }, { gateway, device }) {
      delete device.gateway
      await axios.put(`/api/devices/${device.id}/${gateway.id}`, device)
        .then((res) => {
          if (res.status === 200) {
            dispatch('getAllGateways')
          }
        })
    },
    async deleteDevice ({ dispatch }, device) {
      await axios.delete(`/api/devices/${device.id}`)
        .then((res) => {
          if (res.status === 200) {
            dispatch('getAllGateways')
          }
        })
    },
    selectGateway ({ commit }, gateway) {
      commit(SET_SELECTED, gateway)
    }
  },
  modules: {
  }
})
